import React from 'react'
import './Visualization.css'
import { NavLink } from 'react-router-dom'
import axios from 'axios';
import { Grid, Table, Icon, Segment, Checkbox } from 'semantic-ui-react'
import UGVA_man from './UGVA-man/UGVA-man'

class Visualization extends React.Component {
 state={
        matrix: [],
        curriculum_disciplines: [],
        skills_ugva: [],

        curriculum_name: "",
        curriculum_year: "",
        curriculum_degree: "",

        UGVA_delta: 0,
        skills: [],

        checked: true,

        color_s1: "#FFFFFF", color_s2: "#FFFFFF",
        color_s3: "#FFFFFF", color_s4: "#FFFFFF",
        color_s5: "#FFFFFF", color_show: "#FF0000"
    }

    handleCheck = () => {
        this.setState({checked: !this.state.checked})
    }


    componentDidMount(){

        /* Получаем матрицу смежности */

        /* Делаем запрос одновременно */

            const ourId = this.props.match.params.id

            axios.all([
                axios.get('https://aesu.ru/api/ugva/curriculum/'+ourId+'/matrix'),
                axios.get('https://aesu.ru/api/ugva/curriculum/'+ourId),
                axios.get('https://aesu.ru/api/ugva/skills'),

            ])

        .then (axios.spread((...responses) => {
            /*  Ответ матрицы смежности */
            const matrix = responses[0].data
            /* Ответ УП */
            const curriculum = responses[1].data
            const skills_ugva = responses[2].data

            /* ВСЕ РАСЧЕТЫ ЗДЕСЬ */

            const disciplines = curriculum.disciplines
            let hours_total = 0

            /* Общее количество часов */
            for (let i = 0; i < disciplines.length; i++) {
                hours_total += disciplines[i].audit_hours
            }

            let matrix_update = [...matrix].map(elem => {

                let _elem = Object.assign({}, elem)
                    _elem.estimate = (100 * _elem.discipline.audit_hours / hours_total * _elem.estimate).toFixed(4);
                    _elem.estimate = parseFloat(_elem.estimate);

                    _elem.discipline.control_id === 2 ? _elem.estimate = _elem.estimate : _elem.estimate = _elem.estimate * 0.75 ;
                return (
                    _elem
                )
            })

            let estimate_total = 0

            /* общая оценка */
            for (let i = 0; i < matrix_update.length; i++) {
                estimate_total += matrix_update[i].estimate
            }
            estimate_total = parseFloat(estimate_total.toFixed(4))

            const skills = {}

            for (let i = 0; i < skills_ugva.length; i++){
                skills[skills_ugva[i].id] = {
                    base_total_estimate: 0,
                    variable_total_estimate: 0,
                    name: skills_ugva[i].name
                }
            }

            for (let i = 0; i < matrix_update.length; i++) {

                    matrix_update[i].discipline.part_id === 1
                        ?   skills[matrix_update[i].skill.id].base_total_estimate += matrix_update[i].estimate
                        :   skills[matrix_update[i].skill.id].variable_total_estimate += matrix_update[i].estimate

            }

            let table_skills = skills

            const skills_percent = skills

                for (let key in skills) {
                    skills[key].variable_total_estimate *= 100 / estimate_total;
                    skills[key].base_total_estimate *= 100 / estimate_total;

                    skills[key].base_total_estimate
                        = parseFloat(skills_percent[key].base_total_estimate.toFixed(4))

                    skills[key].variable_total_estimate
                        = parseFloat(skills[key].variable_total_estimate.toFixed(4))
                }

            this.setState({
                skills: skills,
                curriculum_name: curriculum.full_name,
                curriculum_year: curriculum.year,
                curriculum_degree: curriculum.degree.name,
            })


        }))

        .catch(errors => {
            console.log('$errors',errors);
            console.log('$errors.response', errors.response);
        })
    }


     render() {

     const skills_percent = this.state.skills
     const curriculum_name = this.state.curriculum_name
     const curriculum_year =  this.state.curriculum_year
     const curriculum_degree =  this.state.curriculum_degree
     const UGVA_delta = this.state.UGVA_delta

     const data_name = {}
     const data_variable = {}
     const data_base = {}
     const data_sum = {}

     const color_s1 = this.state.color_s1
     const color_s2 = this.state.color_s2
     const color_s3 = this.state.color_s3
     const color_s4 = this.state.color_s4
     const color_s5 = this.state.color_s5
     const color_show = this.state.color_show

     var show_elem

     if (this.state.checked) {
        show_elem = "true"
     } else {
        show_elem = "false"
     }

        for (let i in skills_percent){
                data_name[i] = skills_percent[i].name
                data_variable[i] = skills_percent[i].variable_total_estimate / 2
                data_base[i] = skills_percent[i].base_total_estimate
                data_sum[i] = data_variable[i] + data_base [i]
            }

        const delta = parseFloat(((Math.abs(25 - data_sum[1]) + Math.abs(25 - data_sum[2] - data_sum[3]) +
                    Math.abs(25 - data_sum[4] - data_sum[6]) + Math.abs(25 - data_sum[5])) / 4).toFixed(4))

                    let data_base_2_sum = data_base[2] + data_base[3]
                    let data_variable_2_sum = data_variable[2] + data_variable[3]
                    let data_4_sum = data_base[5] + data_variable[5]
                    let data_5_sum = data_base[6] + data_variable[6]

        // Выводим все на экран
        return (

        <Grid columns={2}>
            <Grid.Column>

                <NavLink
                    exact
                    to="/curriculums"
                    className="ui button basic compact"
                    style={{ marginBottom: "16px" }} >Назад</NavLink>

                <Grid columns='equal'>
                    <Grid.Row>
                        <Grid.Column width={ 4 }>
                            <h5>Учебный план:</h5>
                        </Grid.Column>
                        <Grid.Column>
                            <h5>{curriculum_name}</h5>
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row>
                        <Grid.Column width={ 2 }>
                            <h4>Год:</h4>
                        </Grid.Column>
                        <Grid.Column>
                            <h5>{curriculum_year}</h5>
                        </Grid.Column>

                        <Grid.Column width={ 4 }>
                            <h5>Ступень образования:</h5>
                        </Grid.Column>
                        <Grid.Column>
                            <h5>{curriculum_degree}</h5>
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row>
                        <h5>Оценка сбалансированности (d) =  {delta} </h5>
                    </Grid.Row>

                    <Grid.Row>
                    <h5>Отрегулировать "улыбку" в зависимости от d: </h5>
                        <Grid.Column>
                        <input type="number"
                                min="-5"
                                max="5"
                                value={this.state.UGVA_delta}
                                onChange={event => this.setState({UGVA_delta: event.target.value})}/>
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row>
                        <h5> Показать элементы "нулевые" элементы </h5>
                        <Grid.Column>
                            <Checkbox
                                defaultChecked = {this.state.checked}
                                onChange = {this.handleCheck}
                                toggle/>
                        </Grid.Column>
                        <Grid.Column>
                            <input type="color"
                                value={this.state.color_show}
                                onChange={event => this.setState({color_show: event.target.value})} />
                        </Grid.Column>

                    </Grid.Row>

                    <Table basic="very" unstackable>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell width={ 7 } textAlign="center">Наименование ПУ</Table.HeaderCell>
                                <Table.HeaderCell width={ 4 }>База</Table.HeaderCell>
                                <Table.HeaderCell width={ 4 }>Вариатив</Table.HeaderCell>
                                <Table.HeaderCell width={ 2 }>Цвет</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <Table.Body>
                            <Table.Row>
                                <Table.Cell textAlign="left">S1. {data_name[1]}</Table.Cell>
                                <Table.Cell>{data_base[1]}</Table.Cell>
                                <Table.Cell>{data_variable[1]}</Table.Cell>
                                <Table.Cell>
                                    <input type="color"
                                        value={this.state.color_s1}
                                        onChange={event => this.setState({color_s1: event.target.value})} />
                                </Table.Cell>
                            </Table.Row>

                            <Table.Row>
                                <Table.Cell textAlign="left">S2.1. {data_name[2]}</Table.Cell>
                                <Table.Cell>{data_base_2_sum}</Table.Cell>
                                <Table.Cell>{data_variable_2_sum}</Table.Cell>
                                <Table.Cell>
                                    <input type="color"
                                        value={this.state.color_s2}
                                        onChange={event => this.setState({color_s2: event.target.value})} />
                                </Table.Cell>

                            </Table.Row>

                            <Table.Row>
                                <Table.Cell textAlign="left">S2.2. {data_name[3]}</Table.Cell>
                                <Table.Cell />
                                <Table.Cell />
                                <Table.Cell />
                            </Table.Row>

                            <Table.Row>
                                <Table.Cell textAlign="left">S3. {data_name[4]}</Table.Cell>
                                <Table.Cell>{data_base[4]}</Table.Cell>
                                <Table.Cell>{data_variable[4]}</Table.Cell>
                                <Table.Cell>
                                    <input type="color"
                                        value={this.state.color_s3}
                                        onChange={event => this.setState({color_s3: event.target.value})} />
                                </Table.Cell>
                            </Table.Row>

                            <Table.Row>
                                <Table.Cell textAlign="left">S4. {data_name[5]}</Table.Cell>
                                <Table.Cell>{data_4_sum}</Table.Cell>
                                <Table.Cell></Table.Cell>
                                <Table.Cell>
                                    <input type="color"
                                        value={this.state.color_s4}
                                        onChange={event => this.setState({color_s4: event.target.value})} />
                                </Table.Cell>

                            </Table.Row>

                            <Table.Row>
                                <Table.Cell textAlign="left">S5. {data_name[6]}</Table.Cell>
                                <Table.Cell>{data_5_sum}</Table.Cell>
                                <Table.Cell></Table.Cell>
                                <Table.Cell>
                                    <input type="color"
                                        value={this.state.color_s5}
                                        onChange={event => this.setState({color_s5: event.target.value})} />
                                </Table.Cell>
                            </Table.Row>

                        </Table.Body>
                    </Table>

                </Grid>


            </Grid.Column>

            <Grid.Column>
                <UGVA_man UGVA_data = {skills_percent}
                          UGVA_delta = {UGVA_delta}
                          color_s1 = {color_s1}  color_s2 = {color_s2}
                          color_s3 = {color_s3}  color_s4 = {color_s4}
                          color_s5 = {color_s5}  color_show = {color_show}
                          show_elem = {show_elem}

                          />
            </Grid.Column>
        </Grid>

        )
     }

}

export default Visualization
