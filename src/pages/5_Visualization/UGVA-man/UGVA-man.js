import './UGVA-man.css'
import React from 'react';
import Sketch from "react-p5";
import p5Types from "p5";
import P5Wrapper from 'react-p5-wrapper';
import { Button } from 'semantic-ui-react';


    /* Проверка визуализации */
function UGVA_man(props) {

        const ResData = props.UGVA_data


const skills = []

    const setup = (p5, canvasParentRef) => {
        p5.createCanvas(resultWidth(), resultHeight_max()).parent(canvasParentRef)
    }

    let save_state_eps = false
    let save_state_jpg = false

    const neck_height = 10

        let canvas = null

        // MAIN VARIABLES WITH DEFAULT VALUES
        let hat_size = 45

        let head_radius = 45
        let smile_coef = - props.UGVA_delta

        let body_l1_width = 80
        let body_l1_height = 50

        let body_l2_width = 50
        let body_l2_height = 50

        let body_l3_width = 90
        let body_l3_height = 20

        let body_l4_width = 90
        let body_l4_height = 20

        let arm_left_height = 25

        let arm2_left_width = 60
        let arm3_left_width = 10

        let arm_right_height = 25

        let arm2_right_width = 80
        let arm3_right_width = 10


        let leg1_width = 30
        let leg1_height = 80

        let leg2_width = 30
        let leg2_height = 80

        let leg3_width = 30
        let leg3_height = 80

        //Max variables
        let hat_max = 60
        let head_max = 80

        let body_l1_max = 90
        let body_l2_max = 85
        let body_l3_max = 50
        let body_l4_max = 50

        let arm2_max = 80
        let arm3_max = 70

        let leg_max = 180

        let color_hat = props.color_s4
        let color_arm3_left = props.color_s1
        let color_arm2_left = props.color_s1
        let color_arm3_right = props.color_s3
        let color_arm2_right = props.color_s3
        let color_body_l1 = props.color_s2
        let color_body_l2 = props.color_s2
        let color_body_l3 = props.color_s5

        console.log("ddd", props.UGVA_data)


        for (let key in props.UGVA_data) {

            if (key === '1') {
                arm2_left_width = props.UGVA_data[key].base_total_estimate
                arm3_left_width = (props.UGVA_data[key].variable_total_estimate / 2)

                if (props.show_elem === "true" && props.UGVA_data[key].base_total_estimate === 0) {
                    arm2_left_width = 10
                    color_arm2_left = props.color_show
                }
                else if (props.show_elem === "true" && props.UGVA_data[key].variable_total_estimate === 0) {
                    arm3_left_width = 10
                    color_arm3_left = props.color_show
                }
            }

            else if (key === '2' || key === '3') {
                body_l2_width = (props.UGVA_data[2].variable_total_estimate / 2) +
                                (props.UGVA_data[3].variable_total_estimate / 2)

                body_l1_width = props.UGVA_data[2].base_total_estimate + props.UGVA_data[3].base_total_estimate

                if (props.show_elem === "true" && props.UGVA_data[2].variable_total_estimate === 0 &&
                                                  props.UGVA_data[3].variable_total_estimate === 0  ) {
                    body_l2_width = 10
                    color_body_l2 = props.color_show
                }
                else if (props.show_elem === "true" && props.UGVA_data[2].base_total_estimate === 0 &&
                                                       props.UGVA_data[3].base_total_estimate === 0) {
                    body_l1_width = 10
                    color_body_l1 = props.color_show
                }

            }

            else if (key === '4') {
                arm2_right_width = props.UGVA_data[key].base_total_estimate
                arm3_right_width = (props.UGVA_data[key].variable_total_estimate / 2)

                if (props.show_elem === "true" && props.UGVA_data[key].base_total_estimate === 0) {
                    arm2_right_width = 10
                    color_arm2_right = props.color_show
                }
                else if (props.show_elem === "true" && props.UGVA_data[key].variable_total_estimate === 0) {
                    arm3_right_width = 10
                    color_arm3_right = props.color_show
                }
            }

            else if (key === '5') {
                hat_size = props.UGVA_data[5].base_total_estimate + (props.UGVA_data[5].variable_total_estimate / 2)

                let UGVA_data_5 = props.UGVA_data[key].base_total_estimate + props.UGVA_data[key].variable_total_estimate

                if (props.show_elem === "true" && UGVA_data_5 === 0) {
                    hat_size = 20
                    color_hat = props.color_show
                }
            }

            else if (key === '6') {
                body_l3_width = props.UGVA_data[6].base_total_estimate + (props.UGVA_data[6].variable_total_estimate / 2)
                body_l3_height = body_l3_width

                if (props.show_elem === "true" && props.UGVA_data[6].base_total_estimate === 0
                                               && props.UGVA_data[6].variable_total_estimate === 0) {
                    body_l3_height = body_l3_width = 10
                    color_body_l3 = props.color_show
                }

                leg1_width = 0; leg1_height = 0
                leg2_width = 0; leg2_height = 0
                leg3_width = 0; leg3_height = 0
                body_l4_width = 0; body_l4_height = 0
            }
         }

//         console.log("1b:", arm2_left_width, "  ,1v:", arm3_left_width)
//         console.log("2b:", body_l1_width, "  ,2v:", body_l2_width)
//         console.log("3b:", arm2_right_width, "  ,3v:", arm3_right_width)
//         console.log("hat:", hat_size)
//         console.log("5b:", body_l3_width, "  ,5v:", body_l3_height)

     const resultWidth = () => {
	return (100 + head_max * 2 + (arm3_max + arm2_max) * 2)

}

// TOTAL HEIGHT COUNT METHOD
const resultHeight = () => {
	return 10 + hat_size + head_radius + body_l1_height + body_l2_height + body_l3_height + body_l4_height +
	    Math.max([leg1_height, leg2_height, leg3_height]) + hat_size * 0.10 + neck_height
}


const resultHeight_max = () => {
    return 10 + hat_max + head_max + body_l1_max + body_l2_max + body_l3_max + body_l4_max + leg_max + (hat_max * 0.10) + neck_height
}
        let indent = (resultHeight_max() - resultHeight()) / 2;
        console.log("indent", resultHeight);

    const draw = p5 => {

        p5.rect(5, 5, resultWidth() - 10, resultHeight_max() - 10)

        p5.translate(p5.width / 2, p5.height / 5)

        if (hat_size >= 0) {
            p5.fill(color_hat)

            p5.rect(-hat_size / 2, 0, hat_size, hat_size / 2)
            p5.translate(0, hat_size / 2)

            p5.rect(-hat_size * 0.9, 0, hat_size * 1.8, hat_size / 10)
            p5.translate(0, hat_size / 10 + head_radius / 2)

            p5.fill("#FFFFFF")
            //HAT'S PENT
        }


        // HEAD

        p5.ellipse(0, 0, head_radius)
        p5.ellipse(-head_radius / 5, -head_radius / 5, 6)
        p5.ellipse(head_radius / 5, -head_radius / 5, 6)
        //nose
        p5.line(0, 5, -5, 0)
        p5.line(0, 5, 5, 0)

        p5.translate(0, head_radius * 0.25)

        //smile


        p5.line(-head_radius / 8, 0, head_radius / 8, 0)
        p5.line(-head_radius / 8, 0, -head_radius / 4 , smile_coef)
        p5.line(head_radius / 8, 0, head_radius / 4 , smile_coef)

        p5.translate(0, head_radius * 0.25)

        // NECK
        p5.rect(-10, 0, 20, neck_height)

        p5.translate(0, neck_height)

        // BODY LEVEL 1
        p5.fill(color_body_l1)
        p5.rect(-body_l1_width / 2, 0, body_l1_width, body_l1_height)
        p5.fill("#FFFFFF")

        // HANDS
        //Hands_level1
        p5.fill(props.color_s1)
        p5.rect(-body_l1_width / 2, 0, -10, arm_left_height)
        p5.fill("#FFFFFF")
        p5.fill(props.color_s3)
        p5.rect(body_l1_width / 2, 0, 10, arm_right_height)
        p5.fill("#FFFFFF")


        //Hands_level2

        const point_arm2_left = body_l1_width * 0.5
        const point_arm2_right = body_l1_width * 0.5

        p5.fill(color_arm2_right)
        if (arm2_right_width === 0) { }
        else {
        p5.beginShape();
        p5.vertex (body_l1_width / 2 + 10, 0)
        p5.vertex(body_l1_width / 2 + 10 + arm2_right_width, body_l1_width * 0.5)
        p5.vertex(body_l1_width / 2 + 10 + arm2_right_width, body_l1_width * 0.5 + arm_right_height)
        p5.vertex(body_l1_width / 2 + 10, arm_right_height)
        p5.vertex(body_l1_width / 2 + 10, 0)
        p5.endShape();
        }
        p5.fill("#FFFFFF")

        p5.fill(color_arm2_left)
        if (arm2_left_width === 0) { }
        else {
        p5.beginShape();
        p5.vertex (-body_l1_width / 2 - 10, 0)
        p5.vertex(-body_l1_width / 2 - 10 - arm2_left_width, body_l1_width * 0.5)
        p5.vertex(-body_l1_width / 2 - 10 - arm2_left_width, body_l1_width * 0.5 + arm_left_height)
        p5.vertex(-body_l1_width / 2 - 10, arm_left_height)
        p5.vertex(-body_l1_width / 2 - 10, 0)
        p5.endShape();
        }
        p5.fill("#FFFFFF")

            //Hands_level3

        p5.fill(color_arm3_right)
        p5.rect(body_l1_width / 2 + 10 + arm2_right_width, point_arm2_right, arm3_right_width, arm_right_height)
        p5.fill("#FFFFFF")
        p5.fill(color_arm3_left)
        p5.rect(-body_l1_width / 2 - 10 - arm2_left_width, point_arm2_left , -arm3_left_width, arm_left_height)
        p5.fill("#FFFFFF")

            //Hands_level4
        p5.fill(props.color_s3)
        p5.rect(body_l1_width / 2 + 10 + arm2_right_width + arm3_right_width, point_arm2_right, 15, arm_right_height)
        p5.fill("#FFFFFF")
        p5.fill(props.color_s1)
        p5.rect(-body_l1_width / 2 - 10 - arm2_left_width - arm3_left_width, point_arm2_left, -15, arm_left_height)
        p5.fill("#FFFFFF")

        p5.translate(0, body_l1_height)

        // BODY LEVEL 2
        p5.fill(color_body_l2)
        p5.rect(-body_l2_width / 2, 0, body_l2_width, body_l2_height)
        p5.fill("#FFFFFF")

        p5.translate(0, body_l2_height)

        // BODY LEVEL 3
        p5.fill(color_body_l3)
        p5.rect(-body_l3_width / 2, 0, body_l3_width, body_l3_height)
        p5.fill("#FFFFFF")

        p5.translate(0, body_l3_height)

        // BODY LEVEL 4
        p5.rect(-body_l4_width / 2, 0, body_l4_width, body_l4_height)

        p5.translate(0, body_l4_height)

        //LEGS
        if (leg1_width >= 10){
            p5.rect(-body_l4_width / 2.2, 0, -leg1_width, leg1_height)
        }
        if (leg2_width >= 10){
            p5.rect(-leg2_width / 2, 0, leg2_width, leg2_height)
        }
        if (leg3_width >= 10){
            p5.rect(body_l4_width / 2.2, 0, leg3_width, leg3_height)
        }

        if (save_state_eps === true){
            p5.saveCanvas('ugva_eps', 'eps');
            save_state_eps = false
        }
        if (save_state_jpg === true){
            p5.saveCanvas('ugva_jpg', 'jpg');
            save_state_jpg = false
        }

    }



    return (

    <div>
        <Sketch setup={setup} draw={draw} />
        <Button color='teal' onClick={event => save_state_eps = true}> Сохранить (eps)</Button>
        <Button color='teal' onClick={event => save_state_jpg = true}> Сохранить (jpg)</Button>
    </div>

    )


}

export default UGVA_man
