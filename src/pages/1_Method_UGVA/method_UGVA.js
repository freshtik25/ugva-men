

import React, { Component } from 'react';
import {Grid, Image, Divider, List} from 'semantic-ui-react'

class method_UGVA extends Component {

  render() {

    return (

        <Grid padded={'horizontally'}>
            <Grid.Row>
                <h3> О методе UGVA</h3>
            </Grid.Row>

            <Grid.Row columns={2}>
                <Grid.Column stretched textAlign={'justified'} >
                    <article draggable={'true'}>
                        <p>  Метод "унифицированного графического воплощения активности" (Unified Graphic Visualization of Activity).
                        UGVA (УГВА) - это метод визуального представления профессионального профиля подготовки, выраженного
                        в виде антропоморфного образа, с целью показать особенности его содержания и быстрого сравнения профилей
                        между собой. Пример визуализации в нотации UGVA представлен на рисунке 1.</p>

                        <p>В основу метода UGVA легли подходы когнитивной графики - "бодикодер" Филимонова,
                         "фейкодер" Джоунса, лица Чернова. Методы когнитивной графики позволяют эффективно визализировать
                         многомерные данные для их последующего анализа.
                        </p>

                        <p>Метод UGVA может применяться для широкого круга задач:</p>
                            <ol>
                                <li>сравнение учебных программ внутри ВУЗа, на предмет выявления их перспектив,
                                балансировки их содержания и выделения новых направлений развития;</li>
                                <li>сравнение учебных программ между ВУЗами, на предмет выявления их конкурентных
                                преимуществ и обоснования открытия новых программ подготовки;</li>
                                <li>сравнение профиля подготовки отдельных учащихся как на этапе обучения, так и на
                                этапе профотбора;</li>
                                <li>сравнение специфики реализации подготовки при специализации в рамках конкретной УП
                                ВУЗа;</li>
                                <li>формирование профиля образовательной подготовки индивида </li>
                                <li>выявление трендов и новых направлений профессий.</li>
                        </ol>
                        <Divider section />
                        <h5>Для использования метода UGVA предлагается ознакомиться со следующими статьями:</h5>
                        <Divider />

                        <List>
                            <List.Item>
                                <List.Icon name='file pdf outline' />
                                <List.Content>
                                    <a href='/files/articles/article_UGVA.pdf' download> Метод
                                    унифицированного графического воплощения активности. В.А. Углев </a>
                                </List.Content>
                            </List.Item>
                            <List.Item>
                                <List.Icon name='file pdf outline' />
                                <List.Content>
                                    <a href='/files/articles/RAI-2019-173-181.pdf' download>Модель
                                    для оценки баланса учебной нагрузки методом UGVA для подготовки специалистов
                                    в области ИИ. В.А. Углев, М.В. Некрасов </a>
                                </List.Content>
                            </List.Item>
                            <List.Item>
                                <List.Icon name='file pdf outline' />
                                <List.Content>
                                    <a href='/files/articles/RAI-2020-220-228.pdf' download> Оценка
                                    баланса нагрузки по учебной программе магистратуры направления
                                    "системный анализ и управление" методом UGVA. В.А. Углев, Д.А. Проценко </a>
                                </List.Content>
                            </List.Item>
                        </List>
                    </article>
                </Grid.Column>

                <Grid.Column>
                    <Image src='/files/images/ugva-man_1.png' size='medium' bordered rounded/>
                    <i>Рисунок 1. Визуальный образ в нотации UGVA</i>
                </Grid.Column>
            </Grid.Row>
        </Grid>
    )
  }
}

export default method_UGVA
