import './Curriculum_create.css';
import { Container, Button, Form, Header, Grid, TextArea, Dropdown, Modal } from 'semantic-ui-react';
import axios from 'axios';
import React from 'react';
import { NavLink } from 'react-router-dom'

class Curriculum_create extends React.Component {
    state = {
        name: '',
        year: '',
        about: '',
        degreeId: '',
        specialityId: '',

        specialities: [],
        degrees: []
    }


    submitHandler = event => {
        const { name, year, about, degreeId, specialityId } = this.state

        const dataValues = {
            name: name,
            year: year,
            about: about,
            degreeId: degreeId,
            specialityId: specialityId
        }

         console.log(dataValues);

        axios.get('https://aesu.ru/api/curriculum', { params: dataValues })
        .then(({ data }) => {
            console.log('$ data', data)
        })
        .catch(error => {
            console.log('$ error', error)
            console.log('$ error.response', error.response)
        })
    }


    componentDidMount() {

    axios.get('https://aesu.ru/api/ugva/standards')
        .then (result =>{
            const data = result.data;
            this.setState({
                specialities: data
            }); //текущее состояние изменяем
        })
        .catch(error=>{
            console.log('$error',error);
            console.log('$error.response', error.response);
        })

    axios.get('https://aesu.ru/api/ugva/degrees')
        .then (result =>{
            const data = result.data;
            this.setState({
                degrees: data
            }); //текущее состояние изменяем
        })
        .catch(error=>{
            console.log('$error',error);
            console.log('$error.response', error.response);
        })
    }

    render() {

        let arraySpecialities = this.state.specialities

        arraySpecialities = arraySpecialities.map(element=>{
            return {
                value: element.id,
                key: element.id,
                text: element.name
            }
        })


        let arrayDegrees = this.state.degrees

        arrayDegrees = arrayDegrees.map(element=>{
            return {
                value: element.id,
                key: element.id,
                text: element.name
            }
        })

        const handleDegreesChange = (data, {value}) => {
            this.state.degreeId = value
            console.log(value)
        }

        const handleSpecialitiesChange = (data, {value}) => {
            this.state.specialityId = value
            console.log(value)
        }



        return (
            <Container>

                <Grid>
                    <Grid.Row columns={1}>

                        <Grid.Column> </Grid.Column>

                        <Grid.Column>
                            <Header size='large'> Учебный план (создание) </Header>

                            <Form>
                                <Form.Field>
                                    <label>Наименование</label>
                                    <input
                                        type="text"
                                        placeholder='Наименование'
                                        value={ this.state.name }
                                        onChange={ event => this.setState({ name: event.target.value }) }  />
                                </Form.Field>

                                <Form.Field>
                                    <label>Год набора</label>
                                    <input placeholder='Введите год'
                                        type="number"
                                        min="2000"
                                        max="2030"
                                        value={this.state.year}
                                        onChange={event => this.setState({year: event.target.value})}/>
                                </Form.Field>

                                <Form.Field>
                                    <label>Описание</label>
                                    <TextArea
                                        type="text"
                                        placeholder='Введите описание учебного плана'
                                        value={ this.state.about }
                                        onChange={ event => this.setState({ about: event.target.value }) } />
                                </Form.Field>

                                <Form.Field>
                                    <label>Уровень образования</label>

                                    <Dropdown
                                        placeholder='Выберете уровень образования'
                                        fluid
                                        selection
                                        clearable
                                        options={arrayDegrees}
                                        onChange={ this.handleDegreesChange}
                                     />
                                </Form.Field>

                                 <Form.Field>
                                    <label>Стандарт</label>

                                    <Dropdown
                                        placeholder='Выберете стандарт (Позже специальность)'
                                        fluid
                                        selection
                                        clearable
                                        options={ arraySpecialities }
                                        onChange={ this.handleSpecialitiesChange }
                                     />


                                </Form.Field>
                                    <Modal
                                        trigger={<Button onClick={ this.submitHandler }>Сохранить</Button>}>
                                        <Modal.Header> Уведомление </Modal.Header>
                                        <Modal.Content>
                                             Учебный план успешно создан
                                        </Modal.Content>
                                            <Modal.Actions>

                                            <NavLink
                                                exact
                                                to="/curriculums"
                                                className="ui button basic compact"
                                                style={{ marginBottom: "16px" }} >Ок</NavLink>

                                            </Modal.Actions>
                                    </Modal>

                                    <NavLink
                                    exact
                                    to="/curriculums"
                                    className="ui button basic compact"
                                    style={{ marginBottom: "16px" }} >Назад</NavLink>

                                            </Form>

                                        </Grid.Column>

                    </Grid.Row>
                </Grid>

            </Container>
        )
    }
}

export default Curriculum_create
