import React from 'react'
import { NavLink } from 'react-router-dom'
import { Grid, Table } from 'semantic-ui-react'
import axios from 'axios';

class Curriculum_review extends React.Component {
    state = {
        data: null,
        loading: true
    }

    componentDidMount() {
        // Так мы можем просмотреть все переданные параметры в Route
         console.log('$ this.props.match', this.props.match)
         console.log('$ this.props.match.params', this.props.match.params)

        // Получили переданный :id как параметр в этот компонент
        // Он будет таким же как и указан в Route
        const ourId = this.props.match.params.id

         axios.get('https://aesu.ru/api/ugva/curriculum/' + ourId)
        .then (result =>{
            const data = result.data;
            this.setState({
                data: data,
                loading: false
            }); //текущее состояние изменяем
        })
        .catch(error=>{
            console.log('$error',error);
            console.log('$error.response', error.response);
        })
    }

    render() {
        // Если загрузка, то говорю пользователю об этом
        // (по желанию, можешь не делать)
        if (this.state.loading) return (
            <Grid.Column>
                <p>Загрузка. . .</p>
            </Grid.Column>
        )

        // Получил из состояния данные и отображаю
        const data = this.state.data
        const disciplines = this.state.data.disciplines
        console.log(disciplines)
        let hours_total = 0

        const discipline_table = (
            disciplines.map(function(elem, key){
               let control = "не известно"
               let part = "не известно"

                elem.control_id === 2 ? control = "экзамен" : control = "зачет"
                elem.part_id === 1 ? part = "базовая" : part = "вариативная"

                hours_total += elem.audit_hours


                return(
                    <Table.Row key={ key }>
                        <Table.Cell textAlign="center">{ key + 1 }</Table.Cell>
                        <Table.Cell>{ elem.name }</Table.Cell>
                        <Table.Cell>{ elem.audit_hours }</Table.Cell>
                        <Table.Cell>{ part }</Table.Cell>
                        <Table.Cell>{ control }</Table.Cell>
                    </Table.Row>


                )
            })

        )

        return (
            <Grid columns={2}>
                <Grid.Column>
                    <NavLink
                        exact
                        to="/curriculums"
                        className="ui button basic compact"
                        style={{ marginBottom: "16px" }} >Назад</NavLink>

                    <p>
                        <b>ID: </b> <span>{ data.id }</span>
                    </p>
                    <p>
                        <b>Наименование: </b> <span>{ data.full_name }</span>
                    </p>
                     <p>
                        <b>Год начала обучения: </b> <span>{ data.year }</span>
                    </p>
                    <p>
                        <b>Направление подготовки: </b> <span>{ data.speciality.name }</span>
                    </p>

                    <p>
                        <b>Уровень образования: </b> <span>{ data.degree.name }</span>
                    </p>
                    <p>
                        <b>Университет: </b> <span>{ data.university_id }</span>
                    </p>
                    <p>
                        <b>Описание: </b> <span>{ data.about }</span>
                    </p>


                </Grid.Column>

                <Grid.Column>
                <Table basic="very" unstackable>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell width={ 1 } textAlign="center">№</Table.HeaderCell>
                            <Table.HeaderCell width={ 7 }>Название дисциплины</Table.HeaderCell>
                            <Table.HeaderCell width={ 3 }>Часы</Table.HeaderCell>
                            <Table.HeaderCell width={ 5 }>Часть</Table.HeaderCell>
                            <Table.HeaderCell width={ 1 }> Форма контроля</Table.HeaderCell>

                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        { discipline_table }

                        <Table.Row>
                            <Table.Cell> </Table.Cell>
                            <Table.Cell textAlign="left">Итого</Table.Cell>
                            <Table.Cell>{ hours_total }</Table.Cell>
                            <Table.Cell> </Table.Cell>
                            <Table.Cell> </Table.Cell>
                        </Table.Row>
                    </Table.Body>
                </Table>

                </Grid.Column>
            </Grid>
        )
    }
}
export default Curriculum_review
