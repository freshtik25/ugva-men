import React from 'react'
import './Curriculums.css'
import { Container, Grid} from 'semantic-ui-react';
import {BrowserRouter, Switch, Route, Link, NavLink} from 'react-router-dom'
import axios from 'axios';

import Curriculum_create from './Curriculum_create/Curriculum_create'
import Curriculum_review from './Curriculum_review/Curriculum_review'
import Curriculums_list from './Curriculums_list/Curriculums_list'
import Curriculum_change from './Curriculum_change/Curriculum_change'
import UGVA_man from '../5_Visualization/Visualization'

const Curriculums = props => {
    return (
        <BrowserRouter>
            <Container>
                <Grid style={{MarginTop: "0"    }}>
                    <Grid.Row columns={ 1 }>
                        <Switch>
                            <Route exact path="/curriculum_create" render={ () => ( <Curriculum_create /> ) } />
                            <Route exact path="/curriculum_review/:id" render={ (props) =>
                                ( <Curriculum_review {... props} /> ) } />
                            <Route exact path="/curriculums" render={ () => ( <Curriculums_list /> ) } />
                            <Route exact path="/curriculum_change/:id" render={ (props) =>
                                ( <Curriculum_change {... props} /> ) } />
                            <Route exact path="/visualization/:id" render={ (props) =>
                                ( <UGVA_man {... props} /> ) } />

                        </Switch>
                    </Grid.Row>
                </Grid>
            </Container>
        </BrowserRouter>
    )
}

export default Curriculums
