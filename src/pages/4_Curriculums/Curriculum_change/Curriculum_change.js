import React from 'react'
import { NavLink } from 'react-router-dom'
import axios from 'axios';
import { Container, Button, Form, Header, Grid, TextArea, Dropdown, Modal } from 'semantic-ui-react';


class Curriculum_change extends React.Component {
    state = {
        data: null,
        loading: true,

        name: '',
        year: '',
        about: '',
        degreeId: '',
        standardId: '',

        standards: [],
        degrees: []
    }

    componentDidMount() {
        // Получили переданный :id как параметр в этот компонент
        // Он будет таким же как и указан в Route
        const ourId = this.props.match.params.id

         axios.get('https://aesu.ru/api/curriculum/' + ourId)
        .then (result =>{
            const data = result.data;
            this.setState({
                data: data,
                loading: false
            }); //текущее состояние изменяем
            console.log(data)
        })
        .catch(error=>{
            console.log('$error',error);
            console.log('$error.response', error.response);
        })

        axios.get('https://aesu.ru/api/standards')
        .then (result =>{
            const data = result.data;
            this.setState({
                standards: data
            }); //текущее состояние изменяем
        })
        .catch(error=>{
            console.log('$error',error);
            console.log('$error.response', error.response);
        })

    axios.get('https://aesu.ru/api/degrees')
        .then (result =>{
            const data = result.data;
            this.setState({
                degrees: data
            }); //текущее состояние изменяем
        })
        .catch(error=>{
            console.log('$error',error);
            console.log('$error.response', error.response);
        })
    }


    submitHandler = event => {
        const { name, year, about, degreeId, standardId } = this.state
        const ourId = this.props.match.params.id

        const dataValues = {
            name: name,
            year: year,
            about: about,
            degreeId: degreeId
        }

         console.log(dataValues); return

        axios.get('https://aesu.ru/api/ugva/curriculum/' + ourId + '/update', { params: dataValues })
        .then(({ data }) => {
            console.log('$ data', data)
        })
        .catch(error => {
            console.log('$ error', error)
            console.log('$ error.response', error.response)
            console.log({ params: dataValues })
        })
    }

    render() {
        // Если загрузка, то говорю пользователю об этом
        // (по желанию, можешь не делать)
        if (this.state.loading) return (
            <Grid.Column>
                <p>Загрузка. . .</p>
            </Grid.Column>
        )

        // Получил из состояния данные и отображаю
        const data = this.state.data

                let arrayStandards = this.state.standards

        arrayStandards = arrayStandards.map(element=>{
            return {
                value: element.id,
                key: element.id,
                text: element.name
            }
        })


        let arrayDegrees = this.state.degrees

        arrayDegrees = arrayDegrees.map(element=>{
            return {
                value: element.id,
                key: element.id,
                text: element.name
            }
        })

        const handleDegreesChange = (data, {value}) => {
            this.state.degreeId = value
            console.log(value)
        }

        const handleStandardsChange = (data, {value}) => {
            this.state.standardId = value
            console.log(value)
        }

        return (
            <Container>

                <Grid>
                    <Grid.Row columns={1}>

                        <Grid.Column> </Grid.Column>

                        <Grid.Column>
                            <Header size='large'> Учебный план: {data.name} </Header>

                            <Form>
                                <Form.Field>
                                    <label>Наименование</label>
                                    <input
                                        type="text"
                                        placeholder={data.name}
                                        value={ this.state.name }
                                        onChange={ event => this.setState({ name: event.target.value }) }  />
                                </Form.Field>

                                <Form.Field>
                                    <label>Год набора</label>
                                    <input placeholder={data.year}
                                        type="number"
                                        min="2000"
                                        max="2030"
                                        value={this.state.year}
                                        onChange={event => this.setState({year: event.target.value})}/>
                                </Form.Field>

                                <Form.Field>
                                    <label>Описание</label>
                                    <TextArea
                                        type="text"
                                        placeholder={data.about}
                                        value={ this.state.about }
                                        onChange={ event => this.setState({ about: event.target.value }) }
                                        />
                                </Form.Field>

                                <Form.Field>
                                    <label>Уровень образования</label>

                                    <Dropdown
                                        placeholder='Выберете уровень образования'
                                        fluid
                                        selection
                                        clearable
                                        options={arrayDegrees}
                                        onChange={ this.handleDegreesChange}
                                     />
                                </Form.Field>

                                 <Form.Field>
                                    <label>Специальность</label>



                                </Form.Field>
                                    <Modal
                                        trigger={<Button positive onClick={ this.submitHandler }>Сохранить</Button>}>
                                        <Modal.Header> Уведомление </Modal.Header>
                                        <Modal.Content>
                                             Учебный изменен
                                        </Modal.Content>
                                            <Modal.Actions>

                                            <NavLink
                                                exact
                                                to="/curriculums"
                                                className="ui button basic compact"
                                                style={{ marginBottom: "16px" }} >Ок</NavLink>

                                            </Modal.Actions>
                                    </Modal>

                                    <NavLink
                                    exact
                                    to="/curriculums"
                                    className="ui button"
                                    style={{ marginBottom: "16px" }} >Назад</NavLink>

                                            </Form>

                        </Grid.Column>

                    </Grid.Row>
                </Grid>

            </Container>
        )
    }
}
export default Curriculum_change
