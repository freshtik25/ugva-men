import React from 'react'
import './Curriculums_list.css'
import { NavLink } from 'react-router-dom'
import axios from 'axios';
import { Grid, Table, Icon } from 'semantic-ui-react'


class Curriculums_list extends React.Component {
 state={
        curriculums: []
    }

    componentDidMount(){
        axios.get('https://aesu.ru/api/ugva/curriculums?perPage=100')
        .then (result =>{
            const data = result.data;
            this.setState({
                curriculums: data
            }); //текущее состояние изменяем
        })
        .catch(error=>{
            console.log('$error',error);
            console.log('$error.response', error.response);
        })
    }

        // Отрисовка
    render() {
        // Мапируем наши полученные данные
        const rows = (
            this.state.curriculums.map(function(elem){

                return (
                    <Table.Row key={ elem.id }>
                        <Table.Cell textAlign="center">{ elem.id }</Table.Cell>
                        <Table.Cell>{ elem.name }</Table.Cell>
                        <Table.Cell>{ elem.code }</Table.Cell>
                        <Table.Cell>{ elem.year }</Table.Cell>
                        <Table.Cell>{ elem.university_id }</Table.Cell>
                        <Table.Cell>
                            <NavLink exact to={`/curriculum_review/${elem.id}`}>
                                <Icon link name= 'circle' />info
                            </NavLink>
                        </Table.Cell>
                        <Table.Cell>
                            <NavLink exact to={ `/visualization/${elem.id}` }>
                                UGVA
                            </NavLink>
                        </Table.Cell>
                    </Table.Row>
                )
            })
        )

        // Выводим все на экран
        return (
            <Grid.Column>
                <h3>База данных учебных планов</h3> <h3/>

                <Table  color={'teal'}>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell width={ 1 } textAlign="center">#id</Table.HeaderCell>
                            <Table.HeaderCell width={ 7 }>Название</Table.HeaderCell>
                            <Table.HeaderCell width={ 2 }>Шифр</Table.HeaderCell>
                            <Table.HeaderCell width={ 3 }>Год</Table.HeaderCell>
                            <Table.HeaderCell width={ 5 }>id Университета</Table.HeaderCell>
                            <Table.HeaderCell width={ 1 }></Table.HeaderCell>
                            <Table.HeaderCell width={ 1 }></Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        { rows }
                    </Table.Body>
                </Table>
            </Grid.Column>
        )
    }
}

export default Curriculums_list
