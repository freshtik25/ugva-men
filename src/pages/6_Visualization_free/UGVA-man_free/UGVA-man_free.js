import './UGVA-man_free.css'
import React from 'react';
import Sketch from "react-p5";
import p5Types from "p5";
import P5Wrapper from 'react-p5-wrapper';
import { Button } from 'semantic-ui-react'


    /* Проверка визуализации */
function UGVA_man_free(props) {
    const setup = (p5, canvasParentRef) => {
        p5.createCanvas(resultWidth(), resultHeight_max()).parent(canvasParentRef)
    }

    let save_state_eps = false
    let save_state_jpg = false

    const neck_height = 10

        let canvas = null

        // MAIN VARIABLES WITH DEFAULT VALUES
        let hat_size = props.HEAD.hat_size

        let head_radius = props.HEAD.head_radius
        let smile_coef = - props.HEAD.smile_coef

        let body_l1_width = props.BODY.thorax_width
        let body_l1_height = props.BODY.thorax_height

        let body_l2_width = props.BODY.abdomen_width
        let body_l2_height = props.BODY.abdomen_height

        let body_l3_width = props.PELVIS.pelvis_l1_width
        let body_l3_height = props.PELVIS.pelvis_l1_height

        let body_l4_width = props.PELVIS.pelvis_l2_width
        let body_l4_height = props.PELVIS.pelvis_l2_height

        let arm_left_height = props.ARM.left_height

        let arm2_left_width = props.ARM.shoulder_left_width
        let arm3_left_width = props.ARM.forearm_left_width

        let arm_right_height = +props.ARM.right_height

        let arm2_right_width = +props.ARM.shoulder_right_width
        let arm3_right_width = +props.ARM.forearm_right_width

        let leg1_width = props.LEG.leg1_width
        let leg1_height = props.LEG.leg1_height

        let leg2_width = props.LEG.leg2_width
        let leg2_height = props.LEG.leg2_height

        let leg3_width = props.LEG.leg3_width
        let leg3_height = props.LEG.leg3_height

        //Max variables
        let hat_max = 60
        let head_max = 80

        let body_l1_max = 90
        let body_l2_max = 85
        let body_l3_max = 50
        let body_l4_max = 50

        let arm2_max = 80
        let arm3_max = 70

        let leg_max = 180

        let color_hat = props.HEAD.color_hat
        let color_head = props.HEAD.color_head

        let color_arm3_left = props.ARM.forearm_left_color
        let color_arm2_left = props.ARM.shoulder_left_color
        let color_arm3_right = props.ARM.forearm_right_color
        let color_arm2_right = props.ARM.shoulder_right_color

        let color_body_l1 = props.BODY.thorax_color
        let color_body_l2 = props.BODY.abdomen_color
        let color_body_l3 = props.PELVIS.pelvis_l1_color
        let color_body_l4 = props.PELVIS.pelvis_l2_color

        let color_leg1 = props.LEG.leg1_color
        let color_leg2 = props.LEG.leg2_color
        let color_leg3 = props.LEG.leg3_color

     const resultWidth = () => {
	    return (100 + head_max * 2 + (arm3_max + arm2_max) * 2)
    }

// TOTAL HEIGHT COUNT METHOD
const resultHeight = () => {
	return 10 + hat_size + head_radius + body_l1_height + body_l2_height + body_l3_height + body_l4_height +
	    Math.max([leg1_height, leg2_height, leg3_height]) + hat_size * 0.10 + neck_height
}

const resultHeight_max = () => {
    return 10 + hat_max + head_max + body_l1_max + body_l2_max + body_l3_max + body_l4_max +
    leg_max + (hat_max * 0.10) + neck_height * 10
}
        let indent = (resultHeight_max() - resultHeight()) / 2;
        console.log("indent", resultHeight);

    const draw = p5 => {

        p5.rect(5, 5, resultWidth() - 10, resultHeight_max() - 10)

        p5.translate(p5.width / 2, p5.height / 10)

        if (hat_size >= 0) {
            p5.fill(color_hat)

            p5.rect(-hat_size / 2, 0, hat_size, hat_size / 2)
            p5.translate(0, hat_size / 2)

            p5.rect(-hat_size * 0.9, 0, hat_size * 1.8, hat_size / 10)
            p5.translate(0, hat_size / 10 + head_radius / 2)

            p5.fill("#FFFFFF")
            //HAT'S PENT
        }

        // HEAD

        p5.fill(color_head)
        p5.ellipse(0, 0, head_radius)
        p5.ellipse(-head_radius / 5, -head_radius / 5, 6)
        p5.ellipse(head_radius / 5, -head_radius / 5, 6)
        //nose
        p5.line(0, 5, -5, 0)
        p5.line(0, 5, 5, 0)

        p5.translate(0, head_radius * 0.25)

        //smile

        p5.line(-head_radius / 8, 0, head_radius / 8, 0)
        p5.line(-head_radius / 8, 0, -head_radius / 4 , smile_coef)
        p5.line(head_radius / 8, 0, head_radius / 4 , smile_coef)

        p5.translate(0, head_radius * 0.25)

        // NECK
        p5.rect(-10, 0, 20, neck_height)

        p5.translate(0, neck_height)
        p5.fill("#FFFFFF")

        // BODY LEVEL 1
        p5.fill(color_body_l1)
        p5.rect(-body_l1_width / 2, 0, body_l1_width, body_l1_height)
        p5.fill("#FFFFFF")

        // HANDS
        //Hands_level1
        p5.fill("#FFFFFF")
        p5.rect(-body_l1_width / 2, 0, -10, arm_left_height)
        p5.fill("#FFFFFF")
        p5.fill("#FFFFFF")
//        p5.fill(props.color_s3)
        p5.rect(body_l1_width / 2, 0, 10, arm_right_height)
        p5.fill("#FFFFFF")

        //Hands_level2

        const point_arm2_left = body_l1_width * 0.5
        const point_arm2_right = body_l1_width * 0.5

        p5.fill(color_arm2_right)
        if (arm2_right_width === 0) { }
        else {
        p5.beginShape();
        p5.vertex (body_l1_width / 2 + 10, 0)
        p5.vertex(body_l1_width / 2 + 10 + arm2_right_width, body_l1_width * 0.5)
        p5.vertex(body_l1_width / 2 + 10 + arm2_right_width, body_l1_width * 0.5 + arm_right_height)
        p5.vertex(body_l1_width / 2 + 10, arm_right_height)
        p5.vertex(body_l1_width / 2 + 10, 0)
        p5.endShape();
        }
        p5.fill("#FFFFFF")

        p5.fill(color_arm2_left)
        if (arm2_left_width === 0) { }
        else {
        p5.beginShape();
        p5.vertex (-body_l1_width / 2 - 10, 0)
        p5.vertex(-body_l1_width / 2 - 10 - arm2_left_width, body_l1_width * 0.5)
        p5.vertex(-body_l1_width / 2 - 10 - arm2_left_width, body_l1_width * 0.5 + arm_left_height * 1)
        p5.vertex(-body_l1_width / 2 - 10, arm_left_height)
        p5.vertex(-body_l1_width / 2 - 10, 0)
        p5.endShape();
        }
        p5.fill("#FFFFFF")

            //Hands_level3

        p5.fill(color_arm3_right)
        p5.rect(body_l1_width / 2 + 10 + arm2_right_width, point_arm2_right, arm3_right_width, arm_right_height)
        p5.fill("#FFFFFF")
        p5.fill(color_arm3_left)
        p5.rect(-body_l1_width / 2 - 10 - arm2_left_width, point_arm2_left , -arm3_left_width, arm_left_height)
        p5.fill("#FFFFFF")

            //Hands_level4
        p5.fill("#FFFFFF")
        p5.rect(body_l1_width / 2 + 10 + arm2_right_width + arm3_right_width, point_arm2_right, 15, arm_right_height)
        p5.fill("#FFFFFF")
        p5.rect(-body_l1_width / 2 - 10 - arm2_left_width - arm3_left_width, point_arm2_left, -15, arm_left_height)
        p5.fill("#FFFFFF")

        p5.translate(0, body_l1_height)

        // BODY LEVEL 2
        p5.fill(color_body_l2)
        p5.rect(-body_l2_width / 2, 0, body_l2_width, body_l2_height)
        p5.fill("#FFFFFF")

        p5.translate(0, body_l2_height)

        // BODY LEVEL 3
        p5.fill(color_body_l3)
        p5.rect(-body_l3_width / 2, 0, body_l3_width, body_l3_height)
        p5.fill("#FFFFFF")

        p5.translate(0, body_l3_height)

        // BODY LEVEL 4
        p5.fill(color_body_l4)
        p5.rect(-body_l4_width / 2, 0, body_l4_width, body_l4_height)
        p5.fill("#FFFFFF")

        p5.translate(0, body_l4_height)

        //LEGS
        if (leg1_width >= 10){
            p5.fill(color_leg1)
            p5.rect(-body_l4_width / 2.2, 0, -leg1_width, leg1_height)
            p5.fill("#FFFFFF")
        }
        if (leg2_width >= 10){
            p5.fill(color_leg2)
            p5.rect(-leg2_width / 2, 0, leg2_width, leg2_height)
            p5.fill("#FFFFFF")
        }
        if (leg3_width >= 10){
            p5.fill(color_leg3)
            p5.rect(body_l4_width / 2.2, 0, leg3_width, leg3_height)
            p5.fill("#FFFFFF")
        }

        if (save_state_eps === true){
            p5.saveCanvas('ugva_eps', 'eps');
            save_state_eps = false
        }
        if (save_state_jpg === true){
            p5.saveCanvas('ugva_jpg', 'jpg');
            save_state_jpg = false
        }
    }

    return (
    <div>
        <Sketch setup={setup} draw={draw} />
        <Button color='teal' onClick={event => save_state_eps = true}> Сохранить (eps)</Button>
        <Button color='teal' onClick={event => save_state_jpg = true}> Сохранить (jpg)</Button>
    </div>
    )
}

export default UGVA_man_free
