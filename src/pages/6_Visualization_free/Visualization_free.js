import React from 'react'
import './Visualization_free.css'
import { NavLink } from 'react-router-dom'
import { Grid, Table, Icon, Segment, Checkbox, Form, Input, Button } from 'semantic-ui-react'
import UGVA_man_free from './UGVA-man_free/UGVA-man_free'

class Visualization_free extends React.Component {
 state={
    color_hat : "#FFFFFF",
    hat_size : 45,
    color_head : "#FFFFFF",
    head_radius : 55,
    smile_coef : 0,

    arm_right_height : 25,
    arm_right_color : "#FFFFFF",
    shoulder_right_width : 40,
    shoulder_right_color : "#FFFFFF",
    forearm_right_width : 50,
    forearm_right_color : "#FFFFFF",

    arm_left_height : 25,
    arm_left_color : "#FFFFFF",
    shoulder_left_color : "#FFFFFF",
    forearm_left_width : 50,
    shoulder_left_width : 40,
    forearm_left_color : "#FFFFFF",

    body_color : "#FFFFFF",
    thorax_width : 85,
    thorax_height : 50,
    thorax_color : "#FFFFFF",
    abdomen_width : 50,
    abdomen_height : 40,
    abdomen_color : "#FFFFFF",

    pelvis_l1_width : 80,
    pelvis_l1_height : 20,
    pelvis_l1_color : "#FFFFFF",

    pelvis_l2_width : 85,
    pelvis_l2_height : 30,
    pelvis_l2_color : "#FFFFFF",

    leg1_width : 30,
    leg1_height : 130,
    leg1_color : "#FFFFFF",

    leg2_width : 30,
    leg2_height : 130,
    leg2_color : "#FFFFFF",

    leg3_width : 30,
    leg3_height : 130,
    leg3_color : "#FFFFFF",
    }

      Minimum(){
        this.setState({
            hat_size : 30,
            head_radius : 40,
            smile_coef : -6,

            arm_right_height : 10,
            shoulder_right_width : 15,
            forearm_right_width : 20,
            arm_left_height : 10,
            shoulder_left_width : 15,
            forearm_left_width : 20,

            thorax_width : 40,
            thorax_height : 30,
            abdomen_width : 30,
            abdomen_height : 30,

            pelvis_l1_width : 35,
            pelvis_l1_height : 10,
            pelvis_l2_width : 35,
            pelvis_l2_height : 10,

            leg1_width : 10,
            leg1_height : 40,
            leg2_width : 10,
            leg2_height : 40,
            leg3_width : 10,
            leg3_height : 40,
        })
      }
      Average(){
        this.setState({
            hat_size : 45,
            head_radius : 55,
            smile_coef : 3,

            arm_right_height : 25,
            shoulder_right_width : 40,
            forearm_right_width : 50,
            arm_left_height : 25,
            shoulder_left_width : 40,
            forearm_left_width : 50,

            thorax_width : 85,
            thorax_height : 50,
            abdomen_width : 50,
            abdomen_height : 40,

            pelvis_l1_width : 85,
            pelvis_l1_height : 20,
            pelvis_l2_width : 85,
            pelvis_l2_height : 30,

            leg1_width : 30,
            leg1_height : 130,
            leg2_width : 30,
            leg2_height : 130,
            leg3_width : 30,
            leg3_height : 130,
        })
      }
      Maximum(){
        this.setState({
            hat_size : 60,
            head_radius : 80,
            smile_coef : 6,

            arm_right_height : 40,
            shoulder_right_width : 80,
            forearm_right_width : 70,
            arm_left_height : 40,
            shoulder_left_width : 80,
            forearm_left_width : 70,

            thorax_width : 120,
            thorax_height : 90,
            abdomen_width : 120,
            abdomen_height : 85,

            pelvis_l1_width : 120,
            pelvis_l1_height : 50,
            pelvis_l2_width : 120,
            pelvis_l2_height : 50,

            leg1_width : 50,
            leg1_height : 180,
            leg2_width : 50,
            leg2_height : 180,
            leg3_width : 50,
            leg3_height : 180,
        })
      }
      Color_del(){
        this.setState({
            color_hat : "#FFFFFF",
            color_head : "#FFFFFF",
            arm_right_color : "#FFFFFF",
            shoulder_right_color : "#FFFFFF",
            arm_left_color : "#FFFFFF",
            shoulder_left_color : "#FFFFFF",
            forearm_left_color : "#FFFFFF",
            forearm_right_color : "#FFFFFF",
            body_color : "#FFFFFF",
            thorax_color : "#FFFFFF",
            abdomen_color : "#FFFFFF",
            pelvis_l1_color : "#FFFFFF",
            pelvis_l2_color : "#FFFFFF",
            leg1_color : "#FFFFFF",
            leg2_color : "#FFFFFF",
            leg3_color : "#FFFFFF",
        })
      }

     render() {

     const head = {
        color_hat : this.state.color_hat,
        hat_size : this.state.hat_size,

        head_radius : this.state.head_radius,
        color_head : this.state.color_head,

        smile_coef : this.state.smile_coef,
      }

     const arms = {
        right_height : this.state.arm_right_height,
        shoulder_right_width : this.state.shoulder_right_width,
        shoulder_right_color : this.state.shoulder_right_color,
        forearm_right_width : this.state.forearm_right_width,
        forearm_right_color : this.state.forearm_right_color,

        left_height : this.state.arm_left_height,
        shoulder_left_width : this.state.shoulder_left_width,
        shoulder_left_color : this.state.shoulder_left_color,
        forearm_left_width : this.state.forearm_left_width,
        forearm_left_color : this.state.forearm_left_color,

      }

      const body = {
        thorax_width : this.state.thorax_width,
        thorax_height : this.state.thorax_height,
        thorax_color : this.state.thorax_color,
        abdomen_width : this.state.abdomen_width,
        abdomen_height : this.state.abdomen_height,
        abdomen_color : this.state.abdomen_color,
      }

      const pelvis = {
        pelvis_l1_width : this.state.pelvis_l1_width,
        pelvis_l1_height : this.state.pelvis_l1_height,
        pelvis_l1_color : this.state.pelvis_l1_color,
        pelvis_l2_width : this.state.pelvis_l2_width,
        pelvis_l2_height : this.state.pelvis_l2_height,
        pelvis_l2_color : this.state.pelvis_l2_color,
      }

      const legs = {
        leg1_width : this.state.leg1_width,
        leg1_height : this.state.leg1_height,
        leg1_color : this.state.leg1_color,

        leg2_width : this.state.leg2_width,
        leg2_height : this.state.leg2_height,
        leg2_color : this.state.leg2_color,

        leg3_width : this.state.leg3_width,
        leg3_height : this.state.leg3_height,
        leg3_color : this.state.leg3_color,
      }

        // Выводим все на экран
        return (

        <Grid columns={2}>
            <Grid.Column>
                <h3>Свободная визуализация</h3>

                <Form widths='equal'>
                    <br />
                    <Form.Group widths='5'>

                        <Form.Input label='Шляпа' type='number'
                            value={this.state.hat_size}
                            min = {30}    max = {60}
                            onChange={event => this.setState({hat_size:event.target.value})} />

                        <Form.Field>
                            <label>Цвет</label>
                            <input type='color' value={this.state.color_hat}
                            onChange={event => this.setState({color_hat:event.target.value})} />
                        </Form.Field>

                        <Form.Input label='Голова' type='number'
                            value={this.state.head_radius}
                            min = {40}    max = {80}
                            onChange={event => this.setState({head_radius:event.target.value})} />

                        <Form.Field>
                            <label>Цвет</label>
                            <input type='color' value={this.state.color_head}
                            onChange={event => this.setState({color_head:event.target.value})} />
                        </Form.Field>

                        <Form.Input label='Улыбка' type='number'
                            value={this.state.smile_coef}
                            min = {-6}    max = {6}
                            onChange={event => this.setState({smile_coef:event.target.value})}  />
                    </Form.Group>

                    <br />

                </Form>

                <Table collapsing basic='very'>
                    <Table.Header>
                        <Table.Row textAlign='center'>
                            <Table.HeaderCell>Часть "тела"</Table.HeaderCell>
                            <Table.HeaderCell>Ширина</Table.HeaderCell>
                            <Table.HeaderCell>Высота</Table.HeaderCell>
                            <Table.HeaderCell>Цвет</Table.HeaderCell>
                            <Table.HeaderCell>Общий цвет</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        <Table.Row>
                            <Table.Cell>Левое плечо</Table.Cell>
                            <Table.Cell textAlign='center'>
                                <Input type='number'
                                value={this.state.shoulder_right_width}
                                min = {15}    max = {80}
                                onChange={event => this.setState({shoulder_right_width:event.target.value})} />
                            </Table.Cell>
                            <Table.Cell textAlign='center'>
                                 <Input type='number'
                                 value={this.state.arm_right_height}
                                min = {10}    max = {40}
                                onChange={event => this.setState({arm_right_height:event.target.value})} />
                            </Table.Cell>
                            <Table.Cell textAlign='center'>
                                <input type='color' value={this.state.shoulder_right_color}
                                onChange={event => this.setState({shoulder_right_color:event.target.value})} />
                            </Table.Cell>
                            <Table.Cell textAlign='center'>
                                <input type='color' value={this.state.arm_right_color}
                                onChange={event => this.setState({forearm_right_color:event.target.value,
                                                            shoulder_right_color:event.target.value,
                                                            arm_right_color:event.target.value})} />
                            </Table.Cell>
                        </Table.Row>

                        <Table.Row>
                            <Table.Cell>Левое предплечье</Table.Cell>
                            <Table.Cell textAlign='center'>
                                <Input type='number'
                                value={this.state.forearm_right_width}
                                min = {20}    max = {70}
                                onChange={event => this.setState({forearm_right_width:event.target.value})} />
                            </Table.Cell>
                            <Table.Cell />
                            <Table.Cell textAlign='center'>
                                <input type='color' value={this.state.forearm_right_color}
                                onChange={event => this.setState({forearm_right_color:event.target.value})} />
                            </Table.Cell>
                            <Table.Cell />
                        </Table.Row>

                        <Table.Row>
                            <Table.Cell>Правое плечо</Table.Cell>
                            <Table.Cell textAlign='center'>
                                <Input type='number'
                                value={this.state.shoulder_left_width}
                                min = {15}    max = {80}
                                onChange={event => this.setState({shoulder_left_width:event.target.value})} />
                            </Table.Cell>
                            <Table.Cell textAlign='center'>
                                 <Input type='number'
                                 value={this.state.arm_left_height}
                                 min = {10}    max = {40}
                                onChange={event => this.setState({arm_left_height:event.target.value})} />
                            </Table.Cell>
                            <Table.Cell textAlign='center'>
                                <input type='color' value={this.state.shoulder_left_color}
                                onChange={event => this.setState({shoulder_left_color:event.target.value})} />
                            </Table.Cell>
                            <Table.Cell textAlign='center'>
                                <input type='color' value={this.state.arm_left_color}
                                onChange={event => this.setState({forearm_left_color:event.target.value,
                                                            shoulder_left_color:event.target.value,
                                                            arm_left_color:event.target.value})} />
                            </Table.Cell>
                        </Table.Row>

                        <Table.Row>
                            <Table.Cell>Правое предплечье</Table.Cell>
                            <Table.Cell textAlign='center'>
                                <Input type='number'
                                value={this.state.forearm_left_width}
                                min = {20}    max = {70}
                                onChange={event => this.setState({forearm_left_width:event.target.value})} />
                            </Table.Cell>
                            <Table.Cell />
                            <Table.Cell textAlign='center'>
                                <input type='color' value={this.state.forearm_left_color}
                                onChange={event => this.setState({forearm_left_color:event.target.value})} />
                            </Table.Cell>
                            <Table.Cell />
                        </Table.Row>

                        <Table.Row>
                            <Table.Cell>Туловище 1 уровень</Table.Cell>
                            <Table.Cell textAlign='center'>
                                <Input type='number'
                                value={this.state.thorax_width}
                                min = {40}    max = {120}
                                onChange={event => this.setState({thorax_width:event.target.value})} />
                            </Table.Cell>
                            <Table.Cell textAlign='center'>
                                 <Input type='number'
                                 value={this.state.thorax_height}
                                min = {30}    max = {90}
                                onChange={event => this.setState({thorax_height:event.target.value})} />
                            </Table.Cell>
                            <Table.Cell textAlign='center'>
                                <input type='color' value={this.state.thorax_color}
                                onChange={event => this.setState({thorax_color:event.target.value})} />
                            </Table.Cell>
                            <Table.Cell textAlign='center'>
                                <input type='color' value={this.state.body_color}
                                onChange={event => this.setState({thorax_color:event.target.value,
                                                              abdomen_color:event.target.value,
                                                              body_color:event.target.value})} />
                            </Table.Cell>
                        </Table.Row>

                        <Table.Row>
                            <Table.Cell>Туловище 2 уровень</Table.Cell>
                            <Table.Cell textAlign='center'>
                                <Input type='number'
                                value={this.state.abdomen_width}
                                min = {30}    max = {120}
                                onChange={event => this.setState({abdomen_width:event.target.value})} />
                            </Table.Cell>
                            <Table.Cell textAlign='center'>
                                 <Input type='number'
                                 value={this.state.abdomen_height}
                                min = {30}    max = {85}
                                onChange={event => this.setState({abdomen_height:event.target.value})} />
                            </Table.Cell>
                            <Table.Cell textAlign='center'>
                                <input type='color' value={this.state.abdomen_color}
                                onChange={event => this.setState({abdomen_color:event.target.value})} />
                            </Table.Cell>
                            <Table.Cell />
                        </Table.Row>

                        <Table.Row>
                            <Table.Cell>Таз 1 уровень</Table.Cell>
                            <Table.Cell textAlign='center'>
                                <Input type='number'
                                value={this.state.pelvis_l1_width}
                                min = {35}    max = {120}
                                onChange={event => this.setState({pelvis_l1_width:event.target.value})} />
                            </Table.Cell>
                            <Table.Cell textAlign='center'>
                                 <Input type='number'
                                 value={this.state.pelvis_l1_height}
                                min = {10}    max = {50}
                                onChange={event => this.setState({pelvis_l1_height:event.target.value})} />
                            </Table.Cell>
                            <Table.Cell textAlign='center'>
                                <input type='color' value={this.state.pelvis_l1_color}
                                onChange={event => this.setState({pelvis_l1_color:event.target.value})} />
                            </Table.Cell>
                            <Table.Cell />
                        </Table.Row>

                        <Table.Row>
                            <Table.Cell>Таз 2 уровень</Table.Cell>
                            <Table.Cell textAlign='center'>
                                <Input type='number'
                                min = {35}    max = {120}
                                value={this.state.pelvis_l2_width}
                                onChange={event => this.setState({pelvis_l2_width:event.target.value})} />
                            </Table.Cell>
                            <Table.Cell textAlign='center'>
                                <Input type='number'
                                value={this.state.pelvis_l2_height}
                                min = {10}    max = {50}
                                onChange={event => this.setState({pelvis_l2_height:event.target.value})} />
                            </Table.Cell>
                            <Table.Cell textAlign='center'>
                                <input type='color' value={this.state.pelvis_l2_color}
                                onChange={event => this.setState({pelvis_l2_color:event.target.value})} />
                            </Table.Cell>
                            <Table.Cell />
                        </Table.Row>

                        <Table.Row>
                            <Table.Cell>Нога 1</Table.Cell>
                            <Table.Cell textAlign='center'>
                                <Input type='number'
                                value={this.state.leg1_width}
                                min = {10}    max = {50}
                                onChange={event => this.setState({leg1_width:event.target.value})} />
                            </Table.Cell>
                            <Table.Cell textAlign='center'>
                                 <Input type='number'
                                 value={this.state.leg1_height}
                                 min = {40}    max = {180}
                                 onChange={event => this.setState({leg1_height:event.target.value})} />
                            </Table.Cell>
                            <Table.Cell textAlign='center'>
                                <input type='color' value={this.state.leg1_color}
                                onChange={event => this.setState({leg1_color:event.target.value})} />
                            </Table.Cell>
                            <Table.Cell />
                        </Table.Row>

                        <Table.Row>
                            <Table.Cell>Нога 2</Table.Cell>
                            <Table.Cell textAlign='center'>
                                <Input type='number'
                                value={this.state.leg2_width}
                                 min = {10}    max = {50}
                                onChange={event => this.setState({leg2_width:event.target.value})} />
                            </Table.Cell>
                            <Table.Cell textAlign='center'>
                                 <Input type='number'
                                 value={this.state.leg2_height}
                                 min = {40}    max = {180}
                                 onChange={event => this.setState({leg2_height:event.target.value})} />
                            </Table.Cell>
                            <Table.Cell textAlign='center'>
                                <input type='color' value={this.state.leg2_color}
                                onChange={event => this.setState({leg2_color:event.target.value})} />
                            </Table.Cell>
                            <Table.Cell />
                        </Table.Row>

                        <Table.Row>
                            <Table.Cell>Нога 3</Table.Cell>
                            <Table.Cell textAlign='center'>
                                <Input type='number'
                                value={this.state.leg3_width}
                                 min = {10}    max = {50}
                                onChange={event => this.setState({leg3_width:event.target.value})} />
                            </Table.Cell>
                            <Table.Cell textAlign='center'>
                                 <Input type='number'
                                 value={this.state.leg3_height}
                                 min = {40}    max = {180}
                                 onChange={event => this.setState({leg3_height:event.target.value})} />
                            </Table.Cell>
                            <Table.Cell textAlign='center'>
                                <input type='color' value={this.state.leg3_color}
                                onChange={event => this.setState({leg3_color:event.target.value})} />
                            </Table.Cell>
                            <Table.Cell />
                        </Table.Row>
                    </Table.Body>
                </Table>
            </Grid.Column>

            <Grid.Column>

                <br />
                 <Button.Group attached='top' color='teal'>
                    <Button onClick={this.Minimum.bind(this)}>Минимум</Button>
                    <Button onClick={this.Average.bind(this)}>По умолчанию</Button>
                    <Button onClick={this.Maximum.bind(this)}>Максимум</Button>
                    <Button onClick={this.Color_del.bind(this)}>Сброс цвета</Button>
                 </Button.Group>
                <br />

                <UGVA_man_free HEAD = {head}
                               ARM = {arms}
                               BODY = {body}
                               PELVIS = {pelvis}
                               LEG = {legs}
                />

            </Grid.Column>
        </Grid>

        )
     }
}

export default Visualization_free
