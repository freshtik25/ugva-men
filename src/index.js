import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import {BrowserRouter, Switch, Route} from 'react-router-dom'
import {Menu, Grid, Segment} from 'semantic-ui-react'

import Method_UGVA from './pages/1_Method_UGVA/method_UGVA'
import Curriculums from './pages/4_Curriculums/Curriculums'
import Visualization from './pages/5_Visualization/Visualization'
import UGVA_man from './pages/5_Visualization/UGVA-man/UGVA-man'
import Visualization_free from './pages/6_Visualization_free/Visualization_free'
import UGVA_man_free from './pages/6_Visualization_free/UGVA-man_free/UGVA-man_free'
import './index.css'


export default class MenuExampleTabular extends Component {
        state = { activeItem: ''}

        handleItemClick = (e, { name }) => this.setState({ activeItem: name })

    render() {

            const { activeItem } = this.state

            console.log({activeItem})

            return (
                <BrowserRouter>
                    <Grid padded={'vertically'}>
                        <Grid.Column width={3}>
                            <Menu fluid vertical tabular>
                                <Menu.Item
                                    name= 'О МЕТОДЕ UGVA'
                                    active={activeItem === 'О МЕТОДЕ UGVA'}
                                    onClick={this.handleItemClick}
                                    href="/method_UGVA"
                                />

                                <Menu.Item
                                    name='БАЗА УЧЕБНЫХ ПЛАНОВ'
                                    active={activeItem === 'БАЗА УЧЕБНЫХ ПЛАНОВ'}
                                    onClick={this.handleItemClick}
                                    href="/curriculums"
                                />

                                <Menu.Item
                                    name='СВОБОДНАЯ ВИЗУАЛИЗАЦИЯ'
                                    active={activeItem === 'СВОБОДНАЯ ВИЗУАЛИЗАЦИЯ'}
                                   onClick={this.handleItemClick}
                                    href="/visualization_free"
                                />
                            </Menu>
                        </Grid.Column>

                        <Grid.Column  width={11}>
                            <Switch>
                                <Route exact path="/method_UGVA" component={Method_UGVA} />
                                <Route exact path="/curriculums" component={Curriculums} />
                                <Route exact path="/visualization_free" component={Visualization_free} />
                            </Switch>
                        </Grid.Column>
                    </Grid>
                </BrowserRouter>

            )}
}

ReactDOM.render(
<MenuExampleTabular />  ,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
